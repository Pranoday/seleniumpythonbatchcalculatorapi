from Calculator import Calculator

def testAdditionWithPositiveNumbers(SendPositiveNumbers):
    Cal = Calculator()
    Res=Cal.Addition(SendPositiveNumbers[0],SendPositiveNumbers[1]);
    assert Res==SendPositiveNumbers[2]



def testAdditionWithPositiveAndNegativeNumbers(SendPositiveNegativeNumbersUsingDictionary):
    Cal = Calculator()
    Res = Cal.Addition(SendPositiveNegativeNumbersUsingDictionary["Number1"], SendPositiveNegativeNumbersUsingDictionary["Number2"]);
    assert Res == SendPositiveNegativeNumbersUsingDictionary["Result"]

def testAdditionWithZeroNumbers(CreateObj):
    #Cal = Calculator()
    Res = CreateObj.Addition(0, 0);
    assert Res == 1
