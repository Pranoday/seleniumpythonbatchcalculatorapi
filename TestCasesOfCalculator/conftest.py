import pytest

from Calculator import Calculator
@pytest.fixture()
def CreateObj():
    Cal= Calculator()
    return Cal

@pytest.fixture()
def PositiveNumbers():
    print("Sending Positive Numbers")
    #Returning list of numbers
    return [1000,200,50]

@pytest.fixture(params=[(3000,1000,4000), (1,1,2),(40,45,85),(10000,20000,30000)])
def SendPositiveNumbers(request):
    #return request.param returns single tuple from params with each iteration
    return request.param


@pytest.fixture(params=[{"Number2":-1000,"Number1":3000,"Result":2000}, {"Number1":1,"Number2":-1,"Result":0},{"Number1":-40,"Number2":45,"Result":5}])
def SendPositiveNegativeNumbersUsingDictionary(request):
    #return request.param returns single tuple from params with each iteration
    return request.param
