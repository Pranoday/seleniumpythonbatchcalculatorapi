

def testDivisionWithPositiveNumbers(CreateObj):
    #Cal = Calculator()
    Res=CreateObj.Division(100,20)
    assert Res==5

def testDivisionWithNonzeroAndZeroNumber(CreateObj):
    #Cal = Calculator()
    Res = CreateObj.Division(100, 0)
    assert Res == "You can not divide by zero"
